<?php
/**
 * @file
 * LG Announcement Overlay method.
 */

/**
 * Implments announcement overlay
 */
function lg_announcement_overlay() {
	$options = get_option( 'lg_announcement_options' );
	?>
	<?php if ( isset( $options['lg_announcement_show'] ) ) : ?>
		<?php if ( $options['lg_announcement_show'] ) : ?>
			<?php if ( 'overlay' === $options['lg_announcement_type'] ) : ?>
				<?php if ( isset( $options['lg_announcement_content'] ) && ! empty( $options['lg_announcement_content'] ) ) : ?>

				<div id="lg-ann-overlay" class="lg-ann-overlay" style="display: none">
					<div class="container">
						<?php echo $options['lg_announcement_content']; ?>
					</div>
					<div class="lg-ann-overlay__close closeOverlay">X</div>
					<div class="lg-ann-overlay__button text-center">
						<button class="closeOverlay btn btn-primary">Close</button>
					</div>
				</div>
				<div class="lg-ann-overlay__trigger">
					<?php
					if ( isset( $options['lg_announcement_trigger_button'] ) and ! empty( $options['lg_announcement_trigger_button'] ) ) {
						echo $options['lg_announcement_trigger_button'];
					} else {
						echo 'Announcement';
					}
					?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
	<?php endif; ?>
		<?php
}

if ( ! isset( $_COOKIE['lg_announcement'] ) ) {
	// $cookie_domain = str_replace('www', '', $_SERVER['HTTP_HOST']);
	// setcookie('lg_announcement', '1', time() + (15 * 60), "/", $cookie_domain); // 15 min
}
	add_action( 'wp_footer', 'lg_announcement_overlay' );

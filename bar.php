<?php


function lg_announcement_bar() {
	$lg_ann_bar_style = '';
	$options          = get_option( "lg_announcement_options" );
	if ( isset( $options['lg_announcement_bar_bg_color'] ) and ! empty( $options['lg_announcement_bar_bg_color'] ) ) {
		$lg_ann_bar_style .= 'background-color: ' . $options['lg_announcement_bar_bg_color'] . '!important;';
	}
	if ( isset( $options['lg_announcement_bar_text_color'] ) and ! empty( $options['lg_announcement_bar_text_color'] ) ) {
		$lg_ann_bar_style .= 'color: ' . $options['lg_announcement_bar_text_color'] . '!important;';
		$lg_ann_bar_style .= '--color: ' . $options['lg_announcement_bar_text_color'] . ';';
	}

	?>
	<?php if ( isset( $options['lg_announcement_show'] ) and $options['lg_announcement_show'] ): ?>
		<?php if ( $options['lg_announcement_type'] == "bar" ): ?>
			<?php if ( isset( $options['lg_announcement_content'] ) and ! empty( $options['lg_announcement_content'] ) ) : ?>
				<div class="lg-ann-bar bg-secondary justify-content-center d-flex"
				     style="<?php echo $lg_ann_bar_style ?>">
					<?php echo $options['lg_announcement_content'] ?>
				</div>
			<?php endif; ?>
		<?php elseif ( $options['lg_announcement_type'] == "banner" ): ?>
			<?php if ( isset( $options['lg_announcement_banner_image'] ) and ! empty( $options['lg_announcement_banner_image'] ) ) : ?>
				<div class="lg-ann-bar lg-ann-bar--banner">
					<?php
					if ( wp_is_mobile() ):
						?>
						<img class="lg_announcement_banner_image_small" alt="banner"
						     src="<?php echo $options['lg_announcement_banner_image_small']; ?>"/>
					<?php else: ?>
						<img class="lg_announcement_banner_image" alt="banner"
						     src="<?php echo $options['lg_announcement_banner_image']; ?>"/>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
	<?php
}

add_action( 'lg_announcement_bar', 'lg_announcement_bar' );

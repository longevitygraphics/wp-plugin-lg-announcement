<?php
/**
 * Plugin Name: LG Announcement
 * Plugin URI: https://www.longevitygraphics.com/
 * Description: LG Announcement overlay
 * Version: 1.0
 * Author: Gurpreet Dhanju, Stefan Adam
 * Author URI: https://www.longevitygraphics.com/
 */

require_once plugin_dir_path(__FILE__) . "settings.php";
require_once plugin_dir_path(__FILE__) . "overlay.php";
require_once plugin_dir_path(__FILE__) . "bar.php";


function lg_announcement_add_styles()
{
	wp_register_style('lg_announcement_stylesheet', plugins_url('style.css', __FILE__));
	wp_enqueue_style('lg_announcement_stylesheet');
	wp_register_script('lg_announcement_script', plugins_url('script.js', __FILE__), array('jquery'));
	wp_enqueue_script('lg_announcement_script');

}

add_action('wp_enqueue_scripts', 'lg_announcement_add_styles');


function lg_announcement_add_admin_script($hook) {
	// Add the color picker css file
	wp_enqueue_style( 'wp-color-picker' );

	wp_enqueue_script('my_custom_script', plugin_dir_url(__FILE__) . '/admin-script.js', array('wp-color-picker'), false, true);
}

add_action('admin_enqueue_scripts', 'lg_announcement_add_admin_script');


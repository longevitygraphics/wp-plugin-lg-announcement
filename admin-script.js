jQuery(document).ready(function () {

  jQuery("#text-color-picker").wpColorPicker();
  jQuery("#bg-color-picker").wpColorPicker();


  var announcementType = jQuery("input[name='lg_announcement_options[lg_announcement_type]']:checked").val();
  lgAnnouncementShowHide(announcementType);

  jQuery("input[name='lg_announcement_options[lg_announcement_type]']").change(function () {
    var announcementType = jQuery("input[name='lg_announcement_options[lg_announcement_type]']:checked").val();
    lgAnnouncementShowHide(announcementType);
  });

  function lgAnnouncementShowHide(announcementType) {
    var lg_announcement_row_trigger_button = jQuery(".lg_announcement_row_trigger_button");
    var lg_announcement_row_bar_text_color = jQuery(".lg_announcement_row_bar_text_color");
    var lg_announcement_row_bar_bg_color = jQuery(".lg_announcement_row_bar_bg_color");
    var lg_announcement_row_banner_image = jQuery(".lg_announcement_row_banner_image");
    var lg_announcement_row_banner_image_small = jQuery(".lg_announcement_row_banner_image_small");
    var lg_announcement_row_content = jQuery(".lg_announcement_row_content");

    if (announcementType === 'overlay') {
      lg_announcement_row_banner_image.hide();
      lg_announcement_row_banner_image_small.hide();
      lg_announcement_row_bar_text_color.hide();
      lg_announcement_row_bar_bg_color.hide();
      lg_announcement_row_content.show();
      lg_announcement_row_trigger_button.show();
    }
    if (announcementType === 'bar') {
      lg_announcement_row_banner_image.hide();
      lg_announcement_row_banner_image_small.hide();
      lg_announcement_row_bar_text_color.show();
      lg_announcement_row_bar_bg_color.show();
      lg_announcement_row_content.show();
      lg_announcement_row_trigger_button.hide();
    }
    if (announcementType === 'banner') {
      lg_announcement_row_banner_image.show();
      lg_announcement_row_banner_image_small.show();
      lg_announcement_row_bar_text_color.hide();
      lg_announcement_row_bar_bg_color.hide();
      lg_announcement_row_content.hide();
      lg_announcement_row_trigger_button.hide();
    }
  }

});

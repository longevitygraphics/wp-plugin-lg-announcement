// Windows Ready Handler

(function ($) {
  function setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 60 * 60 * 1000 * days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
  }

  function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
  }


  $(document).ready(function () {

    if (getCookie('lg_announcement')) {

    } else {
      $('.lg-ann-overlay').show();
      setCookie('lg_announcement', '1', 1);
    }
    $(".closeOverlay").click(function () {
      $('.lg-ann-overlay').slideUp();
    });
    $(".lg-ann-overlay__trigger").click(function () {
      $('.lg-ann-overlay').slideToggle();
      if (window.innerWidth < 768) {
        $(window).scrollTop(0);
      }
    });

  });
})(jQuery);

<?php

/**
 * custom option and settings
 */
function lg_announcement_settings_init() {
	// register a new setting for "lg_announcement" page
	register_setting( 'lg_announcement', 'lg_announcement_options' );

	// register a new section in the "lg_announcement" page
	add_settings_section(
		'lg_announcement_section_content',
		__( 'Announcement Settings', 'lg_announcement' ),
		'lg_announcement_section_content_cb',
		'lg_announcement'
	);

	add_settings_field(
		'lg_announcement_show',
		__( 'Show Announcement', 'lg_announcement' ),
		'lg_announcement_show_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_type',
			'class'                       => 'lg_announcement_row_show',
			'lg_announcement_custom_data' => 'custom',
		]
	);

	// register a new field in the "lg_announcement_section_content" section, inside the "lg_announcement" page
	add_settings_field(
		'lg_announcement_type',
		__( 'Announcement Type', 'lg_announcement' ),
		'lg_announcement_type_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_type',
			'class'                       => 'lg_announcement_row_type',
			'lg_announcement_custom_data' => 'custom',
		]
	);

	add_settings_field(
		'lg_announcement_trigger_button',
		__( 'Announcement Trigger Button', 'lg_announcement' ),
		'lg_announcement_trigger_button_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_trigger_button',
			'class'                       => 'lg_announcement_row_trigger_button',
			'lg_announcement_custom_data' => 'custom',
		]
	);

	add_settings_field(
		'lg_announcement_banner_image',
		__( 'Announcement banner image', 'lg_announcement' ),
		'lg_announcement_banner_image_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_banner_image',
			'class'                       => 'lg_announcement_row_banner_image',
			'lg_announcement_custom_data' => 'custom',
		]
	);

	add_settings_field(
		'lg_announcement_banner_image_small',
		__( 'Announcement banner image small', 'lg_announcement' ),
		'lg_announcement_banner_image_small_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_banner_image_small',
			'class'                       => 'lg_announcement_row_banner_image_small',
			'lg_announcement_custom_data' => 'custom',
		]
	);

	add_settings_field(
		'lg_announcement_bar_text_color',
		__( 'Text bar copy color', 'lg_announcement' ),
		'lg_announcement_bar_text_color_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_bar_text_color',
			'class'                       => 'lg_announcement_row_bar_text_color',
			'lg_announcement_custom_data' => 'custom',
		]
	);

	add_settings_field(
		'lg_announcement_bar_bg_color',
		__( 'Text bar background color', 'lg_announcement' ),
		'lg_announcement_bar_bg_color_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_bar_bg_color',
			'class'                       => 'lg_announcement_row_bar_bg_color',
			'lg_announcement_custom_data' => 'custom',
		]
	);


	// register a new field in the "lg_announcement_section_content" section, inside the "lg_announcement" page
	add_settings_field(
		'lg_announcement_content',
		__( 'Announcement content', 'lg_announcement' ),
		'lg_announcement_content_cb',
		'lg_announcement',
		'lg_announcement_section_content',
		[
			'label_for'                   => 'lg_announcement_content',
			'class'                       => 'lg_announcement_row_content',
			'lg_announcement_custom_data' => 'custom',
		]
	);


}

/**
 * register our lg_announcement_settings_init to the admin_init action hook
 */
add_action( 'admin_init', 'lg_announcement_settings_init' );

/**
 * custom option and settings:
 * callback functions
 */

// stripe section cb

// section callbacks can accept an $args parameter, which is an array.
// $args have the following keys defined: title, id, callback.
// the values are defined at the add_settings_section() function.
function lg_announcement_section_content_cb( $args ) {
	//print_r( $args )
	?>
	<p>Announcement Overlay will always work. To use the Announcement Bar, you will need to add the following to the top
		of the header's main section. </p>
	<code>&lt;&#63;php do_action( "lg_announcement_bar") &#63;&#62;</code>
	<?php
}


// field callbacks can accept an $args parameter, which is an array.
// $args is defined at the add_settings_field() function.
// wordpress has magic interaction with the following keys: label_for, class.
// the "label_for" key value is used for the "for" attribute of the <label>.
// the "class" key value is used for the "class" attribute of the <tr> containing the field.
// you can add custom key value pairs to be used inside your callbacks.
function lg_announcement_content_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	$content = isset( $options['lg_announcement_content'] ) ? $options['lg_announcement_content'] : false;
	wp_editor( $content, 'lg_announcement_content', array(
		'textarea_name' => 'lg_announcement_options[' . esc_attr( $args["label_for"] ) . ']',
		'media_buttons' => false,
	) );
}

function lg_announcement_trigger_button_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	$content = isset( $options['lg_announcement_trigger_button'] ) ? $options['lg_announcement_trigger_button'] : false;
	?>
	<input type="text" name="lg_announcement_options[lg_announcement_trigger_button]"
	       value="<?php echo $content ?>"/>
	<?php
}
function lg_announcement_bar_text_color_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	$color = isset( $options['lg_announcement_bar_text_color'] ) ? $options['lg_announcement_bar_text_color'] : false;
	?>
	<input id="text-color-picker" type="text" name="lg_announcement_options[lg_announcement_bar_text_color]"
	       value="<?php echo $color ?>"/>
	<?php
}

function lg_announcement_bar_bg_color_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	$color = isset( $options['lg_announcement_bar_bg_color'] ) ? $options['lg_announcement_bar_bg_color'] : false;
	?>
	<input id="bg-color-picker" type="text" name="lg_announcement_options[lg_announcement_bar_bg_color]"
	       value="<?php echo $color ?>"/>
	<?php
}

function lg_announcement_banner_image_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	$content = isset( $options['lg_announcement_banner_image'] ) ? $options['lg_announcement_banner_image'] : false;
	?>
	<input type="text" name="lg_announcement_options[lg_announcement_banner_image]"
	       style="width: 700px;"
	       value="<?php echo $content ?>"/>
	<?php
}

function lg_announcement_banner_image_small_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	$content = isset( $options['lg_announcement_banner_image_small'] ) ? $options['lg_announcement_banner_image_small'] : false;
	?>
	<input type="text" name="lg_announcement_options[lg_announcement_banner_image_small]"
	       style="width: 700px;"
	       value="<?php echo $content ?>"/>
	<?php
}

function lg_announcement_type_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	?>

	<input type="radio" name="lg_announcement_options[lg_announcement_type]"
	       value="overlay" id="lg_announcement_type_overlay" <?php echo $options["lg_announcement_type"] == "overlay" ? "checked" : "" ?> >
	<label for="lg_announcement_type_overlay">Overlay</label>
	<input type="radio" name="lg_announcement_options[lg_announcement_type]"
	       value="bar" id="lg_announcement_type_bar" <?php echo $options["lg_announcement_type"] == "bar" ? "checked" : "" ?>/>
	<label for="lg_announcement_type_bar">Text Bar</label>
	<input type="radio" name="lg_announcement_options[lg_announcement_type]"
	       value="banner" id="lg_announcement_type_banner" <?php echo $options["lg_announcement_type"] == "banner" ? "checked" : "" ?>/>
	<label for="lg_announcement_type_banner">Banner Image</label>
	<?php
}

function lg_announcement_show_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'lg_announcement_options' );
	//print_r( $options );
	?>

	<input type="checkbox" name="lg_announcement_options[lg_announcement_show]"
	       value="true" <?php echo (isset($options["lg_announcement_show"]) and $options["lg_announcement_show"] == "true") ? "checked" : "" ?> > Show Announcement? </input>
	<?php
}


/**
 * this is necessary for multisite
 */
function lg_announcement_plugins_loaded() {
	/**
	 * register our lg_announcement_options_page to the admin_menu action hook
	 */
	add_action( 'admin_menu', 'lg_announcement_options_page' );
}

add_action( 'plugins_loaded', 'lg_announcement_plugins_loaded' );


/**
 * top level menu
 */
function lg_announcement_options_page() {
	// add top level menu page
	add_menu_page(
		'Announcement Options',
		'Announcement Options',
		'manage_options',
		'lg_announcement',
		'lg_announcement_options_page_html'
	);
}


/**
 * top level menu:
 * callback functions
 */
function lg_announcement_options_page_html() {
	// check user capabilities
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	// add error/update messages

	// check if the user have submitted the settings
	// wordpress will add the "settings-updated" $_GET parameter to the url
	if ( isset( $_GET['settings-updated'] ) ) {
		// add settings saved message with the class of "updated"
		add_settings_error( 'lg_announcement_messages', 'lg_announcement_message', __( 'Settings Saved', 'lg_announcement' ), 'updated' );
	}

	// show error/update messages
	settings_errors( 'lg_announcement_messages' );
	?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<form action="options.php" method="post">
			<?php
			// output security fields for the registered setting "lg_announcement"
			settings_fields( 'lg_announcement' );
			// output setting sections and their fields
			// (sections are registered for "lg_announcement", each field is registered to a specific section)
			do_settings_sections( 'lg_announcement' );
			// output save settings button
			submit_button( 'Save Settings' );
			?>
		</form>
	</div>
	<?php
}
